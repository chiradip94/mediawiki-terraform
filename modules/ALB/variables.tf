variable "name"              { default = "" }
variable "lb_subnets"    { 
                               type = list
                               default = [] 
                             }
variable "security_group"   { 
                               type = list
                               default = [] 
                             }
variable "vpc_id"            { default = "" }

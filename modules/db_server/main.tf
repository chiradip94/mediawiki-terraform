resource "aws_instance" "db" {
  #count                 = var.count
  ami                   = var.ami_id
  instance_type         = var.instance_type
  key_name              = var.key_name
  subnet_id             = var.subnets
  security_groups = var.security_groups
  availability_zone     = var.availability_zone
  root_block_device {
     volume_type = "gp2"
     volume_size = "8"
     delete_on_termination = "true"
        }
  volume_tags = {
     Name                  = "MediaWikiDBv01"
     }
  tags = {
     Name                  = "MediaWikiDB"
     }
  user_data = data.template_file.db_setup.rendered
}


data "template_file" "db_setup" {
  template = file("${path.module}/db_setup.tpl")
  vars = {
    root_db_pw  = var.root_db_pw
    db_name   = var.db_name
    db_un     = var.db_un
    db_pw     = var.db_pw
  }
}


output "private_ip" {
value = aws_instance.db.private_ip
}

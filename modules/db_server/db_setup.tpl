#!/bin/bash
hostnamectl set-hostname dbserver.`hostname -d`
yum install git mariadb-server mariadb -y

###Downloading DB Dump###
git clone https://bitbucket.org/chiradip94/db_dump.git -b v1.0.0-1.26 dump

systemctl start mariadb
echo -e "\n\n${root_db_pw}\n${root_db_pw}\n\n\nn\n\n " | mysql_secure_installation 
echo -e "CREATE USER '${db_un}'@'%.ec2.internal' IDENTIFIED BY '${db_pw}';" | mysql -u root -p${root_db_pw}

mysql -u root -p${root_db_pw} < dump/wikidb.sql

cat << EOF >> db.sql
GRANT ALL PRIVILEGES ON ${db_name}.* TO '${db_un}'@'%.ec2.internal';
FLUSH PRIVILEGES;
SHOW DATABASES;
SHOW GRANTS FOR '${db_un}'@'%.ec2.internal';
EOF

mysql -u root -p${root_db_pw} < db.sql

systemctl enable mariadb

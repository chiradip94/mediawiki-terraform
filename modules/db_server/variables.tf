variable "ami_id"                { default ="" }
variable "instance_type"         { default ="" }
variable "key_name"              { default ="" }
variable "availability_zone"     { default ="" }
variable "subnets"               { default ="" }

variable "security_groups"       { 
type    = list
default =[]
}

### Template Variables ###
variable "db_un"                { default ="" }
variable "db_pw"                { default ="" }
variable "db_name"              { default ="" }
variable "root_db_pw"           { default ="" }


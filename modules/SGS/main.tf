### Resource for security group ####

### VARIABLES ####

variable "vpc_id" { default = "" }
variable "name" { default = "" }

### SECURITY GROUP RESOURCES ###

resource "aws_security_group" "sg" {
name = var.name
vpc_id = var.vpc_id
tags = {
"Name" = var.name
}
}

output "sg_id" {
value = aws_security_group.sg.id
}


### vars
variable "sg_id" { default = "" }
variable "type" { default = "" }
variable "protocol" { default = "" }
variable "from_port" { default = "" }
variable "to_port" { default = "" }
variable "description" { default = "" }
variable "source_security_group_id" { default = "" }

### Resource ###

resource "aws_security_group_rule" "dest_sg" {
  type                     = var.type
  from_port                = var.from_port
  to_port                  = var.to_port
  protocol                 = var.protocol
  source_security_group_id = var.source_security_group_id
  security_group_id        = var.sg_id
  description              = var.description
}

###output###

output "dest_sg_id" {
  value = aws_security_group_rule.dest_sg.id
}

### Variable ###
variable "sg_id" { default = "" }
variable "ip_cidr" {
 type    = list       
 default = [] 
}
variable "type" { default = "" }
variable "protocol" { default = "" }
variable "from_port" { default = "" }
variable "to_port" { default = "" }
variable "description" { default = "" }


### Resource ###
resource "aws_security_group_rule" "dest_ip" {
  type              = var.type
  from_port         = var.from_port
  to_port           = var.to_port
  protocol          = var.protocol
  cidr_blocks       = var.ip_cidr
  security_group_id = var.sg_id
  description       = var.description
}


output "dest_ip_id" {
  value = aws_security_group_rule.dest_ip.id
}


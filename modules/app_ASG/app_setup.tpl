#!/bin/bash

echo "${db_ip} dbserver dbserver.ec2.internal" >> /etc/hosts
rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm
yum -y install puppet-agent
yum -y install git
cd /etc/puppetlabs/code/environments/
git clone https://bitbucket.org/chiradip94/mediawiki-puppet.git -b v1.0.0 control
export FACTER_role="role::mediawikiapp"
export FACTER_puppet_role="mediawikiapp"
export FACTER_db_server="dbserver"
export FACTER_db_name="${db_name}"
export FACTER_db_un="${db_un}"
export FACTER_db_pw="${db_pw}"
export FACTER_ver="${app_ver}"
export FACTER_url="${url}"
export FACTER_site_name="${site_name}"
export FACTER_public_ip=`curl http://169.254.169.254/latest/meta-data/public-ipv4/`
/opt/puppetlabs/bin/puppet apply --environment control /etc/puppetlabs/code/environments/control/manifests/site.pp --hiera_config /etc/puppetlabs/code/environments/control/hiera.yaml

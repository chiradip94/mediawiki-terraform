variable "name"              { default = "" }
variable "ec2_subnets"  { 
                               type = list
                               default = [] 
                             }
variable "security_group"   { 
                               type = list
                               default = [] 
                             }
variable "vpc_id"            { default = "" }
variable "ami_id"            { default = "" }
variable "key_name"          { default = "" }
variable "tg_arn"            { 
                               type = list
                               default = [] 
                             }
variable "instance_type"     { default = "" }
variable "min_size"          { default = "" }
variable "max_size"          { default = "" }


### Template Variables ###
variable "db_ip"                { default ="" }
variable "db_un"                { default ="" }
variable "db_pw"                { default ="" }
variable "db_name"              { default ="" }
variable "site_name"            { default ="" }
variable "app_ver"              { default ="" }
variable "url"                  { default ="" }


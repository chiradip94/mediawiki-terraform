module "db_server" {
source                = "../modules/db_server"
ami_id                = data.aws_ami.amazon.id
instance_type         = var.db_instance_type 
key_name              = var.key_name 
availability_zone     = var.db_availability_zone 
subnets               = var.db_subnet
security_groups       = [module.db_sg.sg_id]
db_un                 = var.db_un
db_pw                 = var.db_pw
db_name               = var.db_name
root_db_pw            = var.root_db_pw

}

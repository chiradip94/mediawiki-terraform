module "LB" {
  source          = "../modules/ALB"
  name            = "MediaWikiLB"
  lb_subnets      = var.lb_subnets
  vpc_id          = var.vpc_id
  security_group  = [ module.lb_sg.sg_id ]
}

module "ASG_a" {
  source          = "../modules/app_ASG"
  name            = "MediaWikiApp"
  ami_id          = data.aws_ami.amazon.id
  key_name        = var.key_name
  ec2_subnets     = var.app_subnets
  vpc_id          = var.vpc_id
  security_group  = [ module.app_sg.sg_id ]
  tg_arn          = [ module.LB.tg_arn ]
  url             = module.LB.LB_DNS
  min_size        = var.min_size_a
  max_size        = var.max_size_a
  db_ip           = module.db_server.private_ip
  db_un           = var.db_un
  db_pw           = var.db_pw
  app_ver         = var.app_ver_a
  db_name         = var.db_name
  site_name       = var.site_name
  instance_type   = var.app_instance_type
}

### This below ASG can be used for rolling update ###

module "ASG_b" {
  source          = "../modules/app_ASG"
  count           = var.max_size_b > 0 ? 1 : 0
  name            = "MediaWikiAppB"
  ami_id          = data.aws_ami.amazon.id
  key_name        = var.key_name
  ec2_subnets     = var.app_subnets
  vpc_id          = var.vpc_id
  security_group  = [ module.app_sg.sg_id ]
  tg_arn          = [ module.LB.tg_arn ]
  url             = module.LB.LB_DNS
  min_size        = var.min_size_b
  max_size        = var.max_size_b
  db_ip           = module.db_server.private_ip
  db_un           = var.db_un
  db_pw           = var.db_pw
  app_ver         = var.app_ver_b
  db_name         = var.db_name
  site_name       = var.site_name
  instance_type   = var.app_instance_type
}


output "ALB_DNS" {
  value = module.LB.LB_DNS
}


variable "vpc_id"                { default ="" }
variable "region"                { default ="us-east-1" }
variable "key_name"              { default ="" }
variable "db_availability_zone"  { default ="" }
variable "db_subnet"             { default ="" }
variable "app_availability_zone" { default ="" }
variable "app_subnets"           { 
                                   type = list
                                   default =[] 
                                 }
variable "lb_subnets"            {
                                   type = list
                                   default =[] 
                                 }
variable "app_instance_type"     { default ="t2.micro" }
variable "db_instance_type"      { default ="t2.micro" }
variable "min_size_a"              { default = "2" }
variable "max_size_a"              { default = "5" }
variable "min_size_b"              { default = "0" }
variable "max_size_b"              { default = "0" }


### Template Variables ###
variable "app_ver_a"              { default ="1.26.4" }
variable "app_ver_b"              { default ="1.26.3" }
variable "db_un"                { default ="" }
variable "db_pw"                { default ="" }
variable "root_db_pw"           { default ="" }
variable "db_name"              { default ="wikidatabase" }
variable "site_name"            { default ="admin" }


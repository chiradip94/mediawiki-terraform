

module "db_sg" {
  source = "../modules/SGS"
  name   = "MediawikiDbSg" 
  vpc_id = var.vpc_id
}

module "app_sg" {
  source = "../modules/SGS"
  name   = "MediawikiAppSg"
  vpc_id = var.vpc_id
}

module "lb_sg" {
  source = "../modules/SGS"
  name   = "MediawikiLbSg" 
  vpc_id = var.vpc_id
}

module "db_sg_rule_1" {
  source = "../modules/SGS/rules/ip"
  from_port = "80"
to_port = "80"
sg_id = module.db_sg.sg_id
protocol = "tcp"
type = "egress"
description = "HTTP internet traffic for DB Server"
ip_cidr = ["0.0.0.0/0"]
}

module "db_sg_rule_2" {
  source = "../modules/SGS/rules/ip"
  from_port = "443"
to_port = "443"
sg_id = module.db_sg.sg_id
protocol = "tcp"
type = "egress"
description = "HTTPS internet traffic for DB Server"
ip_cidr = ["0.0.0.0/0"]
}


module "db_sg_rule_3" {
source = "../modules/SGS/rules/sg"
from_port = "3306"
to_port = "3306"
sg_id = module.db_sg.sg_id
protocol = "tcp"
type = "ingress"
description = "Traffic from App servers"
source_security_group_id = module.app_sg.sg_id
}


### App rules ###

module "app_sg_rule_1" {
  source = "../modules/SGS/rules/ip"
  from_port = "80"
to_port = "80"
sg_id = module.app_sg.sg_id
protocol = "tcp"
type = "egress"
description = "HTTP internet traffic for DB Server"
ip_cidr = ["0.0.0.0/0"]
}

module "app_sg_rule_2" {
  source = "../modules/SGS/rules/ip"
  from_port = "443"
to_port = "443"
sg_id = module.app_sg.sg_id
protocol = "tcp"
type = "egress"
description = "HTTPS internet traffic for App Server"
ip_cidr = ["0.0.0.0/0"]
}


module "app_sg_rule_3" {
source = "../modules/SGS/rules/sg"
from_port = "3306"
to_port = "3306"
sg_id = module.app_sg.sg_id
protocol = "tcp"
type = "egress"
description = "Traffic to DB servers"
source_security_group_id = module.db_sg.sg_id
}


module "app_sg_rule_4" {
source = "../modules/SGS/rules/sg"
from_port = "80"
to_port = "80"
sg_id = module.app_sg.sg_id
protocol = "tcp"
type = "ingress"
description = "Traffic to DB servers"
source_security_group_id = module.lb_sg.sg_id
}


### LB rules ###

module "lb_sg_rule_1" {
  source = "../modules/SGS/rules/ip"
  from_port = "80"
to_port = "80"
sg_id = module.lb_sg.sg_id
protocol = "tcp"
type = "ingress"
description = "HTTP traffic for LB"
ip_cidr = ["0.0.0.0/0"]
}

module "lb_sg_rule_2" {
source = "../modules/SGS/rules/sg"
from_port = "80"
to_port = "80"
sg_id = module.lb_sg.sg_id
protocol = "tcp"
type = "egress"
description = "Traffic from App servers"
source_security_group_id = module.app_sg.sg_id
}


# README #

This repository is to setup MediaWiki Application in a 2 Tier format in AWS.

### Resources it provisions###

* 1 DB Server
* 2 autoscaling groups (2nd ASG is for rolling update and is optional)
* 1 Application Load Balancer

### Prerequisite ###

* 1 VPC
* 2 Subnets in 2 Availability Zones (Atleast).
* terraform version: 0.13.2
* AWS CLI pre-configured with target account

### Steps to execute ###

* cd into "deployment" folder
* terraform init
* terraform apply -var vpc_id="vpc-xxxxxxxxxx" -var db_availability_zone="us-east-1a" -var db_subnet="subnet-xxxxxxxxx" -var lb_subnets='["subnet-xxxxxxxx","subnet-xxxxxxxxx"]' -var app_subnets='["subnet-xxxxxxxx","subnet-xxxxxxxxx"]' -var db_un="wiki" -var db_pw="xxxxxxxxxx" -var root_db_pw="xxxxxx" -var key_name="test" -var region="us-east-1" -out terraform.tfplan
* terraform apply terraform.tfplan


### Working Pattern ###

* This repository contains terraform code that can provision Mediawiki application in high available two tier mode.
* It provisions a single instance for mariadb and sets it up for Mediawiki application. It creates a database for the application and configures it for the application to access.
* It provisions 1 application load balancer, 2 autoscaling groups (2nd one helps in rolling updte by increasing the min_size_b and max_size_b and reducing min_size_a and max_size_a. keeping max_size_b at 0 stops the code from provisioning it) with launch configuration containing user-data to setup the Mediawiki application.
* The application is configured using masterless puppet to use the provisioned db instance and the load balancer as it's URL.
* The required values(DB username and passwords and other sensitive and variables) can be passed as parameters while executing terraform as shown in the "Steps to execute" section.


** To reduce the coplexity, a DB dump created from the manual browser level configuration has been used to setup the DB for ready to use setup.

### Links to the supporting repositories (puppet and db dump) ###

All the reposiories have their latest code in master branch.

* Puppet code repository: https://chiradip94@bitbucket.org/chiradip94/mediawiki-puppet.git
* DB dump repository    : https://chiradip94@bitbucket.org/chiradip94/db_dump.git

### Allowed variables ###

* Variable name         -> Required/Optional   -> Description

===============        =====================  ================

* vpc_id                -> Required            -> The vpc id for the vpc where the complete setup is to be deployed.
* key_name              -> Optional            -> A key name already created in the region if servers are to be verified by logging in.
* db_availability_zone  -> Required            -> The availability zone where the DB server is to be deployed.
* db_subnet             -> Required            -> The subnet Id for DB where it is to be provisioned.
* app_subnets           -> Required            -> The subnet Id for application servers in list format where it is to be provisioned.
* lb_subnets            -> Required            -> The subnet Id for db servers in list format where it is to be provisioned.
* app_instance_type     -> Optional(t2.micro)  -> The instance type for the appplication server, default is t2.micro.
* db_instance_type      -> Optional(t2.micro)  -> The instance type for the database server, default is t2.micro.
* min_size_a            -> Optional(2)         -> The minimum number of servers configured in the primary autoscaling group.
* max_size_a            -> Optional(5)         -> The maximum number of servers configured in the primary autoscaling group.
* min_size_b            -> Optional(0)         -> The minimum number of servers configured in the secondary autoscaling group
* max_size_b            -> Optional(0)         -> The maximum number of servers configured in the secondary autoscaling group. ASG will not be provisioned if this is kept at 0.
* app_ver_a             -> Optional(1.26.4)    -> The version for mediawiki application to be configured. (Tested with and defaults to 1.26.4)
* app_ver_b             -> Optional(1.26.3)    -> The version for mediawiki application to be configured. (Tested with and defaults to 1.26.3)
* db_un                 -> Required            -> The db username for the application to access db.
* db_pw                 -> Required            -> The password for the db user
* root_db_pw            -> Required            -> The password for db user root.
* region                -> Optional(us-east-1) -> The region where resources are to be deployed
